/**
 * Open mobile menu func
 * Watching #menu-toggle and show #mobile-nav-menu
 */
document.getElementById('menu-toggle').addEventListener('click', function() {
  var menu = document.getElementById('mobile-nav-menu');
  if(menu.style.display === 'flex') {
    menu.style.display = 'none';
  } else {
    menu.style.display = 'flex';
  }
});

/**
 * Close mobile menu func
 * Watching #close-menu-toggle and remove #mobile-nav-menu
 */
document.getElementById('close-menu-toggle').addEventListener('click', function() {
  var menu = document.getElementById('mobile-nav-menu');
    menu.style.display = 'none';
});

/**
 * Switch Language
 * Watching #language-switcher <select>
 */
var languageSwitcher = document.getElementById('language-switcher');

// Добавляем обработчик события изменения выбранного пункта
languageSwitcher.addEventListener('change', function () {
    var selectedLanguage = this.value; // Получаем значение выбранного пункта
    langSwitcherRequest(selectedLanguage); // Вызываем функцию смены языка
});

/**
 * Switch language API request function
 * @params
 * lang: string - variable containing 'ru' or 'en' string value
 */
function langSwitcherRequest(lang) {
    console.log('language', lang)
    fetch('', { // URL, чтобы отправить запрос
        method: 'POST', // метод HTTP запроса
        body: JSON.stringify({language: lang}) // ru/en
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
            return response.json(); // или response.text() если ожидается текстовый ответ
        })
        .then(data => {
            console.log(data); // Обработка данных, полученных в результате запроса
        })
        .catch(error => {
            console.error('There has been a problem with your fetch operation:', error);
        });
}

// Language switcher END


// reduce wine item description from catalogue to max N-symbols

document.addEventListener('DOMContentLoaded', function () {
    let maxSymbols = 80
    let paragraphs = document.querySelectorAll('.wine-item-desc'); // Получаем все элементы с классом description
    paragraphs.forEach(function (p) {
        if (p.innerText.length > maxSymbols) {
            p.innerText = p.innerText.substring(0, maxSymbols) + '...'; // Обрезаем текст до 50 символов и добавляем троеточие
        }
    });
});

