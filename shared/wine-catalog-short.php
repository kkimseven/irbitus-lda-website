        <!-- Каталог вин. Для примера поместил два вина. Здесь нужно добавить какой-то запрос на вывод каталога-->

        <section class="wine-catalog">
            <h2 class="wine-catalog-heading">
                <img src="./assets/images/main/star.svg" alt="">
                <span>Винная карта</span>
                <img src="./assets/images/main/star.svg" alt="">
            </h2>

            <div class="wine-catalog-inner">
                <!--        API REQUEST: вывод последних трёх вин - (.wine-item)-->
                <div class="wine-item">
                    <a href="#" class="wine-item-image">
                        <img src="./assets/images/main/wine1.png" alt="">
                        <div class="wine-item-type">Красное</div>
                    </a>
                    <div class="wine-item-title">
                        <a href="#" class="wine-item-name">Pedras Negras</a>
                        <div class="wine-item-percentage">13%</div>
                    </div>
                    <div class="wine-item-desc">
                        Рубиновый цвет, аромат лесных ягод и листьев эвкалипта, полнотелое и тд тп тптптптп тп тп тптп
                    </div>
                </div>

                <div class="wine-item">
                    <a href="#" class="wine-item-image">
                        <img src="./assets/images/main/wine2.png" alt="">
                        <div class="wine-item-type">Белое</div>
                    </a>
                    <div class="wine-item-title">
                        <a href="#" class="wine-item-name">Arezes</a>
                        <div class="wine-item-percentage">12%</div>
                    </div>
                    <div class="wine-item-desc">
                        Желто-цитрусовый цвет, фруктовый аромат и вкус с хорошей кислотностью 123 13 21312 31231 31
                        312321312 3123 1231231
                    </div>
                </div>
            </div>

            <div class="wine-watch-all">
                <div class="line"></div>
                <a href="#">Смотреть все</a>
                <div class="line"></div>
            </div>
        </section>