<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Блог - IRBITUS LDA</title>
    <link rel="stylesheet" href="./styles/styles.css">
    <!--    FONTS START-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400..900;1,400..900&family=Roboto:wght@100;300;400;500;700;900&family=Vollkorn:ital,wght@0,400..900;1,400..900&display=swap"
          rel="stylesheet">
    <!--    FONTS END-->
</head>
<body>
<!-- HEADER section-->
<?php include 'shared/header.php'; ?>
<!-- HEADER section END-->

<!--MAIN section START-->
<div class="main">
    <div class="container">
        <section class="about-hero">
            <h1 class="centered wn-heading">
                <span> <img src="./assets/images/main/star.svg" alt=""></span>
                <span>ВИННЫЙ БЛОГ</span>
                <span><img src="./assets/images/main/star.svg" alt=""></span>
            </h1>
        </section>


        <section class="wine-blog-category">
             <!-- API REQUEST: вывод последних четырех статей - (.wine-blog-category-item) -->
            <div class="wine-blog-category-item">
<!--                entity: <a href> - category_url -->
                <a href="blog_category.php" class="wine-blog-category-item-image">
<!--                    entity: image path -->
                    <img src="assets/images/blog/1.png" alt="">
                </a>
                <div class="wine-blog-category-item-desc">
<!--                    entity: <h2> - category_name, <a href> - category_url -->
                        <h2>
                            <a href="blog_category.php">Виноделие в Португалии</a>
                        </h2>
<!--                    entity: <a href> - category_url-->
                    <a href="blog_category.php"><button class="button transparent">Читать</button></a>
                </div>
            </div>

        </section>

        <!--Блог винных статей-->
        <?php include 'shared/wine-blog-other-articles.php'; ?>


    </div>

    <section class="contact-us">
        <div class="contact-us-card">
            <h2>
                <span>Хотите заказать вино? У вас возникли вопросы? Мы вам поможем!</span>
                <span>
                        <img src="./assets/images/main/star.svg" alt="">
                    </span>
            </h2>
            <form class="contact-us-form" action="">
                <input class="wn-input" type="text" name="name" placeholder="Ваше имя">
                <input class="wn-input" type="email" name="name" placeholder="Ваш E-mail">
                <input class="wn-input" type="email" name="name" placeholder="Сообщение нам">
                <button class="button filled">Отправить</button>
            </form>
            <img src="./assets/images/main/rectangle_33.png" alt="">
        </div>
    </section>
</div>

<!--MAIN section END-->

<!--FOOTER section START-->
<?php include 'shared/footer.php'; ?>
<!--FOOTER section END-->

<!--SCRIPTS-->
<script src="scripts/main.js"></script>
<!--SCRIPTS END-->
</body>
</html>
