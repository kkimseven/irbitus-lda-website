<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Блог - IRBITUS LDA</title>
    <link rel="stylesheet" href="./styles/styles.css">
    <!--    FONTS START-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400..900;1,400..900&family=Roboto:wght@100;300;400;500;700;900&family=Vollkorn:ital,wght@0,400..900;1,400..900&display=swap"
          rel="stylesheet">
    <!--    FONTS END-->
</head>
<body>
<!-- HEADER section-->
<?php include 'shared/header.php'; ?>
<!-- HEADER section END-->

<!--MAIN section START-->
<div class="main">
    <div class="container">
        <section class="wine-article-hero">
            <h1 class="centered wn-heading">
                <span> <img src="./assets/images/main/star.svg" alt=""></span>
                <span>Винная карта</span>
                <span><img src="./assets/images/main/star.svg" alt=""></span>
            </h1>

        </section>

        <!-- Каталог вин. Для примера поместил два вина. Здесь нужно добавить какой-то запрос на вывод каталога-->

        <section class="wine-catalog">


            <div class="wine-catalog-inner">
                <!--        API REQUEST: вывод последних трёх вин - (.wine-item)-->
                <div class="wine-item">
                    <a href="#" class="wine-item-image">
                        <img src="./assets/images/main/wine1.png" alt="">
                        <div class="wine-item-type">Красное</div>
                    </a>
                    <div class="wine-item-title">
                        <a href="#" class="wine-item-name">Pedras Negras</a>
                        <div class="wine-item-percentage">13%</div>
                    </div>
                    <div class="wine-item-desc">
                        Рубиновый цвет, аромат лесных ягод и листьев эвкалипта, полнотелое и тд тп тптптптп тп тп тптп
                    </div>
                </div>

                <div class="wine-item">
                    <a href="#" class="wine-item-image">
                        <img src="./assets/images/main/wine2.png" alt="">
                        <div class="wine-item-type">Белое</div>
                    </a>
                    <div class="wine-item-title">
                        <a href="#" class="wine-item-name">Arezes</a>
                        <div class="wine-item-percentage">12%</div>
                    </div>
                    <div class="wine-item-desc">
                        Желто-цитрусовый цвет, фруктовый аромат и вкус с хорошей кислотностью 123 13 21312 31231 31
                        312321312 3123 1231231
                    </div>
                </div>
            </div>


        </section>
    </div>


</div>

<!--MAIN section END-->

<!--FOOTER section START-->
<?php include 'shared/footer.php'; ?>
<!--FOOTER section END-->

<!--SCRIPTS-->
<script src="scripts/main.js"></script>
<!--SCRIPTS END-->
</body>
</html>
