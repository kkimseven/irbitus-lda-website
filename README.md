## About The Project
This is a website of company "IRBITUS LDA"


## Build with

No frameworks/builders. Developed using HTML+CSS+JS

## Contact
Project owner and Designed by - <a href="https://t.me/svernigor">Сергей Вернигор</a> 
<br>
Developer - <a href="https://t.me/kiko7182">Konstantin Kim</a>