<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>О нас - IRBITUS LDA</title>
    <link rel="stylesheet" href="./styles/styles.css">
    <!--    FONTS START-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400..900;1,400..900&family=Roboto:wght@100;300;400;500;700;900&family=Vollkorn:ital,wght@0,400..900;1,400..900&display=swap"
          rel="stylesheet">
    <!--    FONTS END-->
</head>
<body>
<!-- HEADER section-->
<?php include 'shared/header.php'; ?>
<!-- HEADER section END-->

<!--MAIN section START-->
<div class="main">
    <div class="container about-page">
        <section class="about-hero">
            <h1 class="wine-catalog-heading wn-heading">
                <span> <img src="./assets/images/main/star.svg" alt=""></span>
                <span>Наша история</span>
                <span><img src="./assets/images/main/star.svg" alt=""></span>
            </h1>
            <img src="./assets/images/about/about.png" alt="">
            <p class="centered">
                Приветствуем вас в IRBITUS lda! Мы здесь, чтобы познакомить вас с настоящими португальскими винами.
            </p>
        </section>


        <section class="imgtext-section forward">
            <div class="imgtext-image">
                <img src="./assets/images/about/rectangle_14.png" alt="">
            </div>
            <div class="imgtext-text">
                <h2>
                    <span>Начало пути</span>
                    <span><img src="./assets/images/main/star.svg" alt=""></span>
                </h2>
                <p>
                    Всё началось с простой идеи: познакомить людей со вкусом португальских вин. Я начал с небольшой
                    команды, увлечённой идеей находить и привозить в Украину особенные португальские вина. Шаг за шагом,
                    от первых поставок до международных связей, наша история -
                    это история страсти к качеству и вкусу.
                </p>
            </div>
        </section>

        <section class="imgtext-section reverse">
            <div class="imgtext-image">
                <img src="./assets/images/about/rectangle_13.png" alt="">
            </div>
            <div class="imgtext-text">
                <h2>
                    <span>Вдохновитель IRBITUS lda</span>
                    <span><img src="./assets/images/main/star.svg" alt=""></span>
                </h2>
                <p>
                    До основания IRBITUS lda, я много лет руководил пивоварнями Carlsberg в разных странах, но всегда
                    интересовался вином и любил его. И вот, переехав в Португалию, я понял, что у виноделия в этой
                    стране огромный потенциал.
                </p>
            </div>
        </section>

        <section class="imgtext-section forward">
            <div class="imgtext-image">
                <img src="./assets/images/about/rectangle_12.png" alt="">
            </div>
            <div class="imgtext-text">
                <p>
                    Я решил основать IRBITUS - это название означает на латыни “кисть” - ту, которой пишут картины.
                    Пришлось много ездить и учиться выбирать вина, соответствующие вкусам потребителей и могущие помочь
                    развить винную культуру на традиционно “не-винных” рынках, таких, например, как Украина или
                    Казахстан.
                </p>
            </div>
        </section>

        <section class="imgtext-section wine-choice">
            <div class="imgtext-image">
                <h2>
                    <span>Выбор вина</span>
                    <span><img src="./assets/images/main/star.svg" alt=""></span>
                </h2>

                <p>
                    В IRBITUS мы уделяем особое внимание процессу отбора вин. Мы лично посещаем португальские
                    винодельни,
                    выбирая вина, которые лучше всего соответствуют вкусам и предпочтениям наших клиентов. Этот процесс
                    не просто о выборе качественного продукта, но и о нахождении уникальных вкусовых сочетаний, которые
                    могут предложить эти вина. Это еще и про установление отношений с виноделами, некоторые из которых
                    стали моими хорошими друзьями.
                </p>
                <img class="wine-choice-image" src="./assets/images/about/rectangle_10.png" alt="">
            </div>
            <div class="wine-choice-mobile-image">
                <img src="assets/images/about_mobile/rectangle_15.png" alt="">
                <img src="assets/images/about_mobile/rectangle_16.png" alt="">
            </div>
            <div class="imgtext-text">
                <img class="wine-choice-image" src="./assets/images/about/rectangle_15.png" alt="">
                <h2>
                    <span>Персональный подход</span>
                    <span><img src="./assets/images/main/star.svg" alt=""></span>
                </h2>

                <p>
                    В IRBITUS мы уделяем особое внимание процессу отбора вин. Мы лично посещаем португальские
                    винодельни,
                    выбирая вина, которые лучше всего соответствуют вкусам и предпочтениям наших клиентов. Этот процесс
                    не просто о выборе качественного продукта, но и о нахождении уникальных вкусовых сочетаний, которые
                    могут предложить эти вина. Это еще и про установление отношений с виноделами, некоторые из которых
                    стали моими хорошими друзьями.
                </p>

            </div>
        </section>




        <!--Блог винных статей-->
        <?php include 'shared/wine-blog-short.php'; ?>


    </div>

    <section class="contact-us">
        <div class="contact-us-card">
            <h2>
                <span>Хотите заказать вино? У вас возникли вопросы? Мы вам поможем!</span>
                <span>
                        <img src="./assets/images/main/star.svg" alt="">
                    </span>
            </h2>
            <form class="contact-us-form" action="">
                <input class="wn-input" type="text" name="name" placeholder="Ваше имя">
                <input class="wn-input" type="email" name="name" placeholder="Ваш E-mail">
                <input class="wn-input" type="email" name="name" placeholder="Сообщение нам">
                <button class="button filled">Отправить</button>
            </form>
            <img src="./assets/images/main/rectangle_33.png" alt="">
        </div>
    </section>
</div>

<!--MAIN section END-->

<!--FOOTER section START-->
<?php include 'shared/footer.php'; ?>
<!--FOOTER section END-->

<!--SCRIPTS-->
<script src="scripts/main.js"></script>
<!--SCRIPTS END-->
</body>
</html>
