<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Блог - IRBITUS LDA</title>
    <link rel="stylesheet" href="./styles/styles.css">
    <!--    FONTS START-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400..900;1,400..900&family=Roboto:wght@100;300;400;500;700;900&family=Vollkorn:ital,wght@0,400..900;1,400..900&display=swap"
          rel="stylesheet">
    <!--    FONTS END-->
</head>
<body>
<!-- HEADER section-->
<?php include 'shared/header.php'; ?>
<!-- HEADER section END-->

<!--MAIN section START-->
<div class="main">
    <div class="container">
        <section class="wine-article-hero">
            <h1 class="centered wn-heading">
                <span> <img src="./assets/images/main/star.svg" alt=""></span>
                <span>Вина, которые мы любим</span>
                <span><img src="./assets/images/main/star.svg" alt=""></span>
            </h1>

            <img src="./assets/images/rectangle_45.png" alt="">
        </section>


    </div>

    <div class="article-content">
        <div class="article-container">
            <h3>Погрузитесь в элегантность португальских вин: Сетубал и Винью Верде</h3>
            <p class="article-paragraph">
                В панораме мировых вин некоторые названия являются воплощением исключительного мастерства и
                соблазнительного
                вкуса, и среди них царят вина Setubal и Vinho Verde из Португалии. В [Имя вашего сайта] мы отправляемся
                в
                увлекательное путешествие, чтобы разгадать сложные нюансы и вкусы, которые делают эти вина такими
                уважаемыми
                игроками на энологической арене.
            </p>

            <img src="./assets/images/rectangle_46.png" alt="">

            <h3>Погрузитесь в элегантность португальских вин: Сетубал и Винью Верде</h3>
            <p class="article-paragraph">
                В панораме мировых вин некоторые названия являются воплощением исключительного мастерства и
                соблазнительного
                вкуса, и среди них царят вина Setubal и Vinho Verde из Португалии. В [Имя вашего сайта] мы отправляемся
                в
                увлекательное путешествие, чтобы разгадать сложные нюансы и вкусы, которые делают эти вина такими
                уважаемыми
                игроками на энологической арене.
            </p>
        </div>
    </div>

    <div class="container" style="margin-top: 5rem;">
        <!--Блог винных статей-->
        <?php include 'shared/wine-blog-other-articles.php'; ?>
    </div>

</div>

<!--MAIN section END-->

<!--FOOTER section START-->
<?php include 'shared/footer.php'; ?>
<!--FOOTER section END-->

<!--SCRIPTS-->
<script src="scripts/main.js"></script>
<!--SCRIPTS END-->
</body>
</html>
