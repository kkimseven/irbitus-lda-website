<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная - IRBITUS LDA</title>
    <link rel="stylesheet" href="./styles/styles.css">
    <!--    FONTS START-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400..900;1,400..900&family=Roboto:wght@100;300;400;500;700;900&family=Vollkorn:ital,wght@0,400..900;1,400..900&display=swap"
          rel="stylesheet">
    <!--    FONTS END-->
</head>
<body>
<!-- HEADER section-->
<?php include 'shared/header.php'; ?>
<!-- HEADER section END-->

<!--MAIN section START-->
<div class="main">
    <div class="container">
        <section class="hero">
            <h1 class="hero-heading">Вина из сердца Португалии</h1>
            <div class="hero-image">
                <img src="./assets/images/main/hero.png" alt="">
                <p class="hero-text">
                    Присоединяйтесь к нам в изучении вин,
                    которые отражают историю, культуру и
                    страсть Португалии к виноделию
                </p>

                <button class="button transparent">
                    Заказать консультацию
                </button>
            </div>
        </section>

        <section class="history">
            <div class="col-image">
                <img src="./assets/images/main/history.png" alt="">
            </div>
            <div class="col-text">
                <h2 class="history-heading">
                    <span>Наша история</span>
                    <img src="./assets/images/main/star.svg" alt="">
                </h2>
                <p class="history-text">
                    Добро пожаловать в IRBITUS lda. Основанная Евгением Шевченко, бывшим главой Carlsberg Ukraine,
                    многолетним членом Совета директоров Американской торговой палаты в Украине, Глобального договора
                    ООН и UNIC. Наша компания специализируется на экспорте португальских вин в Украину и другие страны.
                    Мы тщательно выбираем каждое вино, предлагая уникальные вкусы и качество. Наши услуги включают
                    персональные консультации и разнообразные упаковки, соответствующие запросам клиентов. Здесь вы
                    найдете вина, выбранные с заботой и знанием дела.
                </p>
                <button class="button transparent">
                    Узнать подробнее
                </button>
            </div>
        </section>

<!--        Каталог вин-->
        <?php include 'shared/wine-catalog-short.php' ?>

        <section class="portugal">
            <div class="col-1">
                <h2 class="portugal-heading">
                    <span>Виноделие в Португалии</span>
                    <img src="./assets/images/main/star.svg" alt="">
                </h2>
                <p>
                    Португалия славится своими богатыми традициями виноделия, которые охватывают века. Разнообразный
                    климат, уникальные сорта винограда и проверенные временем методы виноделия сделали ее сокровищницей
                    для любителей вина по всему миру.
                </p>

                <img class="portugal-col-image" src="./assets/images/main/portugal1.png" alt="">
            </div>
            <div class="portugal-mobile-image">
                <img src="assets/images/main_mobile/portugal.png" alt="">
            </div>
            <div class="col-2">
                <img class="portugal-col-image" src="./assets/images/main/portugal2.png" alt="">
                <p>
                    Португальское виноделие отличается своим уникальным подходом: разнообразие терруаров позволяет
                    создавать вина с различными вкусовыми нотами, местные сорта винограда (их более 300!) придают вину
                    уникальный характер, старинные традиции сочетаются с инновациями для улучшения качества, а
                    устойчивое использование ресурсов подчеркивает заботу о будущем планеты.
                </p>

                <button class="button transparent">
                    Узнать подробнее
                </button>

            </div>
        </section>

        <!--Блог винных статей-->
        <?php include 'shared/wine-blog-short.php'; ?>


    </div>

    <section class="contact-us">
        <div class="contact-us-card">
            <h2>
                <span>Хотите заказать вино? У вас возникли вопросы? Мы вам поможем!</span>
                <span>
                        <img src="./assets/images/main/star.svg" alt="">
                    </span>
            </h2>
            <form class="contact-us-form" action="">
                <input class="wn-input" type="text" name="name" placeholder="Ваше имя">
                <input class="wn-input" type="email" name="name" placeholder="Ваш E-mail">
                <input class="wn-input" type="email" name="name" placeholder="Сообщение нам">
                <button class="button filled">Отправить</button>
            </form>
            <img src="./assets/images/main/rectangle_33.png" alt="">
        </div>
    </section>
</div>

<!--MAIN section END-->

<!--FOOTER section START-->
<?php include 'shared/footer.php'; ?>
<!--FOOTER section END-->

<!--SCRIPTS-->
<script src="scripts/main.js"></script>
<!--SCRIPTS END-->
</body>
</html>
