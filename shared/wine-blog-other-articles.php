<section class="wine-blog">
            <h2 class="wine-catalog-heading">
                <img src="./assets/images/main/star.svg" alt="">
                <span>Все статьи</span>
                <img src="./assets/images/main/star.svg" alt="">
            </h2>
            <div class="wine-blog-catalog">
                <!-- API REQUEST: вывод всех статей - (.wine-blog-item)-->
                <div class="wine-blog-item">
<!--                    enity: <a href> - article_url -->
                    <a href="../article.php" class="wine-blog-item-image">
<!--                        entity: <img src> - image path -->
                        <img src="./assets/images/main/rect_7.png" alt="">
                    </a>
<!--                    entity: <a href> - article_url -->
                    <a href="#" class="wine-blog-item-name">
                        Винная азбука
                    </a>
                </div>

                <div class="wine-blog-item">
                    <a href="#" class="wine-blog-item-image">
                        <img src="./assets/images/main/rect_10.png" alt="">
                    </a>
                    <a href="#" class="wine-blog-item-name">
                        Изменение климата и вино
                    </a>
                </div>

                <div class="wine-blog-item">
                    <a href="#" class="wine-blog-item-image">
                        <img src="./assets/images/main/rect_12.png" alt="">
                    </a>
                    <a href="#" class="wine-blog-item-name">
                        Вино и бутерброды
                    </a>
                </div>

            <div class="wine-watch-all">
                <div class="line"></div>
                <a href="blog.html">Смотреть все</a>
                <div class="line"></div>
            </div>
        </section>