<footer class="footer">
    <div class="container">
        <div class="footer-inner">
            <div class="footer-nav">
                <div class="footer-nav-col">
                    <a href="#">Политика конфиденциальности</a>
                </div>
                <div class="footer-nav-col">
                    <a href="tel:+351933018008">
                        +351 933 018 008
                    </a>
                    <a href="mailto:evgeny@irbitus.com">evgeny@irbitus.com</a>
                    <a href="#">linkedin.com</a>
                </div>
            </div>
        </div>
    </div>
</footer>