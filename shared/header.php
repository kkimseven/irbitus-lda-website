<header class="header">
    <div class="container">
        <div class="header-inner">
            <div class="header-nav">
                <div class="header-mobile-switcher" id="menu-toggle">
                    <img src="assets/images/burger.svg" alt="">
                </div>
                <div class="header-logo">IRBITUS LDA</div>
                <nav class="header-nav-links">
                    <a href="./" class="nav-link">Главная</a>
                    <a href="wine-catalog.php" class="nav-link-active">Винная карта</a>
                    <a href="about.php" class="nav-link">О нас</a>
                    <a href="blog.php" class="nav-link">Блог</a>
                </nav>
            </div>
            <div class="header-actions">
                <div class="language-switcher">
                    <select id="language-switcher">
                        <option value="ru">Рус</option>
                        <option value="en">En</option>
                    </select>
                </div>

                <button class="button filled small">Заказать вино</button>
            </div>
        </div>
    </div>
</header>


<div class="mobile-nav-menu" id="mobile-nav-menu">
    <div class="close-mobile-nav-menu" id="close-menu-toggle">
        <img src="assets/images/x.svg" alt="">
    </div>
    <div class="mobile-nav-menu-links">
        <a href="./" class="nav-link">Главная</a>
        <a href="wine-catalog.php" class="nav-link-active">Винная карта</a>
        <a href="about.php" class="nav-link">О нас</a>
        <a href="blog.php" class="nav-link">Блог</a>
    </div>

    <div class="language-switcher">
        <select id="language-switcher">
            <option value="ru">Рус</option>
            <option value="en">En</option>
        </select>
    </div>

    <div class="mobile-nav-footer-links">
        <a href="#">Политика конфиденциальности</a>
        <a href="tel:+351933018008">
            +351 933 018 008
        </a>
        <a href="mailto:evgeny@irbitus.com">evgeny@irbitus.com</a>
        <a href="#">linkedin.com</a>
    </div>
</div>